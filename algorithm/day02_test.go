package algorithm

import (
	"fmt"
	"testing"
)

func TestLeftRightBound(t *testing.T) {
	nums := []int{2, 3, 4, 5, 7, 8, 9, 6}
	bound := LeftRightBound(nums)
	fmt.Println(bound)
}

func TestRecoverSearchTree(t *testing.T) {
	nums := []int{2, 3, 4, 5, 7, 8, 9, 6}
	tree := RecoverSearchTree(nums)
	TailTraverse(tree)
}
