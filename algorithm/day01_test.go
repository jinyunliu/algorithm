package algorithm

import (
	"fmt"
	"testing"
)

// AC
func TestRotate(t *testing.T) {
	// 创建一个 3*3 的正方形 要求原地循环90度
	square := [][]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	fmt.Println(square)
	rotate := Rotate(square)
	fmt.Println(rotate)
}

func TestPrintMatrixZigZag(t *testing.T) {
	matrix := [][]int{
		{1, 2, 3},
		{4, 5, 6},
	}

	// 1 2 4 5 3 6
	PrintMatrixZigZag(matrix)
}
