package string

import (
	"fmt"
	"strings"
	"testing"
)

func TestLengthOfLongestDistinctSubstring(t *testing.T) {
	n1 := LengthOfLongestDistinctSubstring("abcabcbb")
	fmt.Println(n1)

	n2 := LengthOfLongestDistinctSubstring("bbbbb")
	fmt.Println(n2)

	n3 := LengthOfLongestDistinctSubstring("pwwkew")
	fmt.Println(n3)

	n4 := LengthOfLongestDistinctSubstring("")
	fmt.Println(n4)

	n5 := LengthOfLongestDistinctSubstring(" ")
	fmt.Println(n5)

	n6 := LengthOfLongestDistinctSubstring("au")
	fmt.Println(n6)

	n7 := LengthOfLongestDistinctSubstring("abba")
	fmt.Println(n7)
}

func TestLongestPalindrome(t *testing.T) {
	var (
		//s0 = "a"
		//s1 = "aba"
		//s2 = "abba"
		s3 = "cbbd"
	)

	//p0 := LongestPalindrome(s0)
	//fmt.Println(p0)
	//
	//p1 := LongestPalindrome(s1)
	//fmt.Println(p1)
	//
	//p2 := LongestPalindrome(s2)
	//fmt.Println(p2)

	p3 := LongestPalindrome(s3)
	fmt.Println(p3)

	fmt.Println(s3[1:2])
	fmt.Println(s3[1:3])
}

func TestConvertZ(t *testing.T) {
	var (
		s       = "ABC"
		numRows = 3
	)

	z := ConvertZ(s, numRows)
	fmt.Println(z)
}

func TestStringsTrim(t *testing.T) {
	s := "   -1"
	prefix := strings.TrimPrefix(s, " ")
	fmt.Println(prefix)

	left := strings.TrimLeft(s, " ")
	fmt.Println(left)
}

func TestString(t *testing.T) {
	var (
		//s = "hello"
		s = "01249Aa"
		// 0 -> 48,   A65  a97
		s0 = ""  // []
		s1 = " " // 32
	)

	for i, b := range []byte(s) {
		fmt.Println(i, b)
	}

	fmt.Println([]byte(s0))
	fmt.Println([]byte(s1))
	integer := '9' - '0'
	fmt.Println(int(integer))

	trim := strings.Trim(s0, s1)
	fmt.Println(trim)
}

func TestEmpty(t *testing.T) {
	s := "  "
	trim := strings.Trim(s, " ")
	fmt.Println(trim)
}

func TestLongestCommonPrefix(t *testing.T) {
	var (
		s0   = "helloworld"
		s1   = "helloworldxxx"
		arr1 = []string{s0, s1}
	)

	prefix := LongestCommonPrefix(arr1)
	fmt.Println(prefix)
}

func TestLetterCombinations(t *testing.T) {
	var (
		digit1 = "23"
	)

	c1 := LetterCombinations(digit1)
	fmt.Println(c1)
}

func TestIsValid(t *testing.T) {
	//s := "{[()]}"
	s := "){"

	valid := IsValid(s)
	fmt.Println(valid)
}

// leetcode28: 实现strStr
func TestStrStr(t *testing.T) {
	i := StrStr("qaab", "aa")
	fmt.Println(i)
}

func TestNexts(t *testing.T) {
	nexts := Next("ababcaabc")
	fmt.Println(nexts)
}

func TestStrStrKMP(t *testing.T) {
	var (
		//h1, n1 = "mississippi", "issip"
		//h2, n2 = "hello", "ll"
		//h3, n3 = "aaaaa", "bba"
		h4, n4 = "ababcaababcaabc", "ababcaabc"
	)

	//k1 := StrStrKMP(h1, n1)
	//fmt.Println(k1)
	//
	//k2 := StrStrKMP(h2, n2)
	//fmt.Println(k2)
	//
	//k3 := StrStrKMP(h3, n3)
	//fmt.Println(k3)

	k4 := StrStrKMP(h4, n4)
	fmt.Println(k4)
}
