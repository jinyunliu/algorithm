package linear

import (
	"fmt"
	"testing"
)

// 找到倒数第k个节点的数

type Node struct {
	Val  int
	Next *Node
}

func TestFindLastK(t *testing.T) {
	node3 := &Node{3, nil}
	node2 := &Node{2, node3}
	head := &Node{1, node2}
	//k := findLastK(3, head)
	//fmt.Println(k)
	node := recursion4Reverse(head)

	printLinkedList(node)
	n := reverse(node)
	printLinkedList(n)
}

// 1 2 3 4 5 6
func findLastK(k int, head *Node) int {
	p := &Node{}
	// 传统计数 第几个是从几开始计数的, 如果是1 下面就应该是1, 反正需要位移 k-1 位, 快指针到K的时候, 满指针要指向链表的头节点开始的位置;
	c := 1
	for p = head; c < k; c++ {
		// 如果给的k 大于了 链表的长度直接返回一个-1 没有找到
		if p == nil {
			return -1
		}

		p = p.Next
	}

	q := head
	for p.Next != nil {
		q = q.Next
		p = p.Next
	}

	return q.Val
}

// 单链表的反转
// 链表求环
// 数组奇偶性排序

// 递归他妈又不会了我草真的难受
// 1 -> 2 -> 3 -> 4 -> 5 -> 6
func recursion4Reverse(head *Node) *Node {
	if head == nil || head.Next == nil {
		return head
	}

	p := head
	subHead := recursion4Reverse(p.Next)
	p.Next.Next = p
	p.Next = nil
	return subHead
}

// what the fuck? 头插 jdk1.7 hashMap 头插法
// 1 -> 2 -> 3 -> 4 -> 5 -> 6
func reverse(head *Node) *Node {
	emptyHeadNode := Node{}
	p := head
	for p != nil {
		//t := p
		if emptyHeadNode.Next == nil {
			emptyHeadNode.Next = p
			// 移动下一个节点 同时将这个新的尾节点置为nil
			p = p.Next
			emptyHeadNode.Next.Next = nil
		} else {
			// 临时保存当前节点 同时后移
			t := p
			p = p.Next
			t.Next = emptyHeadNode.Next
			emptyHeadNode.Next = t
		}
	}

	return emptyHeadNode.Next
}

// echo: node_val
func printLinkedList(head *Node) {
	p := head
	for p != nil {
		fmt.Println(p.Val)
		p = p.Next
	}
}
