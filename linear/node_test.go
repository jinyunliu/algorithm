package linear

import (
	"fmt"
	"testing"
)

var (
	n3 = &ListNode{4, nil}
	n2 = &ListNode{3, n3}
	n1 = &ListNode{2, n2}
	n0 = &ListNode{1, n1}
)

func TestAddTwoNumbers(t *testing.T) {
	node3 := &ListNode{3, nil}
	node2 := &ListNode{2, node3}
	head1 := &ListNode{1, node2}

	head2 := &ListNode{1, node2}

	newHead := AddTwoNumbers(head2, head1)
	newHead.PrintLinkedList()
}

func TestRemoveNthFromEndV2(t *testing.T) {
	node := &ListNode{1, nil}
	node2 := &ListNode{2, node}
	v2 := RemoveNthFromEndV2(node2, 1)
	fmt.Println(v2)
}

func TestSwapPairs(t *testing.T) {
	pairs := SwapPairsV2(n0)
	pairs.PrintLinkedList()
}

func TestReverseKGroup(t *testing.T) {
	group := ReverseKGroup(n0, 3)
	group.PrintLinkedList()
}

func TestReverseK(t *testing.T) {
	group := ReverseK(n0, 2)
	group.PrintLinkedList()
}
