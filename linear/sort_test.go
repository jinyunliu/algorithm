package linear

import (
	"fmt"
	"testing"
)

var (
	numbers = []int{2, 4, 1, 8, 5, 9, 3}
)

func TestSelectSort(t *testing.T) {
	SelectSort(numbers)
	fmt.Println(numbers)
}

func TestBubbleSort(t *testing.T) {
	BubbleSort(numbers)
	fmt.Println(numbers)
}

func TestInsertionSort(t *testing.T) {
	InsertionSort(numbers)
	fmt.Println(numbers)
}

func TestShellSort(t *testing.T) {
	ShellSort(numbers)
	fmt.Println(numbers)
}

// mergeSort 是分治算法的入门, 所以一定要掌握 归并排序 能够知道这个算法的时间复杂度

// divide and conquer;
// 1 2 3 4 5 6 7
// the problem size is n;
// if n is odd,  for example, size = 3, half is = 1. 0 1 2, divided 0 1, 2
// else n is even, size = 4, 0 1; 2 3;           0, size>1 -1          size>1, size;

// 从上面可以发现 需要 lefter and righter bounds, 先二分吗
// 所有的子序列都copy新的进行操作
func mergeSort(arr []int) {
	// divide firstly   [0, mid] && [mid+1, len(arr) - 1]
	sort := MergeSort(arr)
	fmt.Println(sort)
}

// fastSort: a very practical algorithm, 是 BubbleSort 的升级形式
// do not need more space like merge_sort, 原地调整, 时间复杂度 O(n)

// 治理: conquer: easy, recursively sort two subArrays;
// MergeTwoSortedArr:

// Pseudocode:
// 我们对算法进行渐进分析的时候不需要考虑 constants
// 但是在 写代码的时候 you should care about constants;

// 通过一轮循环可以从两边把 pivot: 主元 就是我们之前写的 mid, 专业一点 pivot: 主元, 中心点,[ˈpɪvət], 转抽支点
// 2 1 9 6 7 3 8

// Basically: 基本地;
// That's all it is.
// By the way,  recursion tree;
func fastSort(arr []int) {
	// 这个是在原来的数组上完成操作, 所以应该给出子序列的上下界 method signature, void FastSort([]int, left, right)
	FastSort(arr, 0, len(arr)-1)
}

func TestFastSort(t *testing.T) {
	arr := []int{6, 1, 9, 2}
	fastSort(arr)
	fmt.Println(arr)
}

func TestHeapSort(t *testing.T) {
	arr := []int{4, 6, 8, 5, 9}
	HeapSort(arr)
	fmt.Println(arr)
}
