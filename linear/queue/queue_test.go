package queue

import (
	"fmt"
	"testing"
)

func TestNewCircularQueue(t *testing.T) {
	queue := NewRingQueue()
	fmt.Println("Queue is Empty: ", queue.IsEmpty())
	fmt.Println("Queue is Full: ", queue.IsFull())
	c := "good"
	for _, b := range []byte(c) {
		queue.Enq(b)
	}

	fmt.Println("dequeue one element: ", queue.Deq())
	// byte 就是 0-255 的数字 1111 1111 = 2^8-1 = 255
	queue.Enq('a')

	fmt.Println("Queue is Empty: ", queue.IsEmpty())
	fmt.Println("Queue is Full: ", queue.IsFull())
}
