package linear

import (
	"fmt"
	"testing"
)

var (
	nums   = []int{1, 6, 20}
	target = 7
)

// leetcode1: 两数之和
func TestTwoSum(t *testing.T) {
	r := TwoSum(nums, target)
	fmt.Println(r)
}

// 这个关键之处就是 有序数组, 应该具备什么效果; 有序数组可以使用 二分查找;
func TestIsEven(t *testing.T) {
	even := isEven(5)
	fmt.Println(even)
}

// 判断是不是偶数
func isEven(n int) bool {
	return n|1 != n
}

// 要求优化之后 算法时间复杂度： log(m+n), 但凡时间复杂度有要求, log相关的, 一般都是二分查找;

// 一定要会写归并排序 这个是分而治之 的思想; https://www.cnblogs.com/chengxiao/p/6194356.html
// 回去演算归并排序 将一个大的问分成 若干个子问题  然后解决每个子问题的排序问题 然后逆向 进行子问题的处理
func TestMergeSort(t *testing.T) {
	sort := MergeTwoSortedArr(nums, []int{})
	fmt.Println(sort)
}

func TestVote(t *testing.T) {
	arr := []int{1, 5, 4, 5, 5, 6, 7, 8, 5, 5}
	i := FindMaxWater(arr)
	if i == -1 {
		return
	}

	fmt.Println(arr[i])
}

// 如果面试官 面试的问题 你会了使用一种方法, 那么千万别得意, 并且还是要多沟通, 抓住几点问面试官需要注意的细节;
// 学习编程 除了面试之外 还可以参与自己喜欢的软件开发中 创造自己的价值 利用算法 和  语言特点 构建高性能的 开源软件 这才是最重要的

func TestFindMedianSortedArrays(t *testing.T) {
	arr := []int{1, 2, 3}
	source := []int{8, 10}
	i := FindMedianSortedArrays(arr, source)
	fmt.Println(i)
}

func TestSearchInsert(t *testing.T) {
	arr := []int{1, 3, 5, 6}
	i := SearchInsert(arr, 5)
	fmt.Println(i)

	i1 := SearchInsert(arr, 2)
	fmt.Println(i1)

	i2 := SearchInsert(arr, 7)
	fmt.Println(i2)

	i3 := SearchInsert(arr, 0)
	fmt.Println(i3)

	arr2 := []int{1}
	i5 := SearchInsert(arr2, 0)
	fmt.Println(i5)
}

func TestMaxArea(t *testing.T) {
	arr1 := []int{1, 8, 6, 2, 5, 4, 8, 3, 7}
	arr2 := []int{1, 1}
	arr3 := []int{4, 3, 2, 1, 4}
	arr4 := []int{1, 2, 4, 3}

	area1 := MaxArea(arr1)
	area2 := MaxArea(arr2)
	area3 := MaxArea(arr3)
	area4 := MaxArea(arr4)
	fmt.Println(area1, area2, area3, area4)
}

func TestGenerateParenthesis(t *testing.T) {
	parenthesis := GenerateParenthesis(2)
	fmt.Println(parenthesis)
}

// leetcode26
func TestDelDuplicate(t *testing.T) {
	arr := []int{1, 5, 5, 5, 6, 7, 8}
	noDuplicate := RemoveDuplicates(arr)
	fmt.Println(arr[:noDuplicate])
}

// leetcode27: 移除相等的元素 返回数组的长度
func TestRemoveElement(t *testing.T) {
	arr := []int{1, 5, 5, 5, 6, 7, 8}
	cnt := RemoveElement(arr, 5)
	fmt.Println(arr[:cnt])
}
