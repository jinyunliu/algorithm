package util

func Abs(val int) int {
	if val < 0 {
		return -val
	}

	return val
}

func Max(val1, val2 int) int {
	if val1 < val2 {
		return val2
	} else {
		return val1
	}
}

func Min(val1, val2 int) int {
	if val1 < val2 {
		return val1
	} else {
		return val2
	}
}
