package digit

import (
	"fmt"
	"math"
	"strconv"
	"testing"
)

func TestReverseInteger(t *testing.T) {
	i := ReverseInteger(-123)
	fmt.Println(i)
}

func TestAtoi2(t *testing.T) {
	s := "-2147483647"
	atoi := Atoi(s)
	fmt.Println(atoi)
}

func TestAtoi(t *testing.T) {
	var (
		s0 = "9223372036854775808"
		s1 = "-2147483648"
		s2 = "   -42"
		s3 = "4193 with words"
		s4 = "words and 987"
		s5 = "-91283472332"
		s6 = "+-42"
		s7 = "00000-42a1234"
		s8 = "+1"
	)

	atoi0 := Atoi(s0)
	atoi1 := Atoi(s1)
	atoi2 := Atoi(s2)
	atoi3 := Atoi(s3)
	atoi4 := Atoi(s4)
	atoi5 := Atoi(s5)
	atoi6 := Atoi(s6)
	atoi7 := Atoi(s7)
	atoi8 := Atoi(s8)
	fmt.Println(atoi0, atoi1, atoi2, atoi3, atoi4, atoi5, atoi6, atoi7, atoi8)
}

func TestName(t *testing.T) {
	var (
		i = 0
	)

	fmt.Println(i + math.MaxInt32)
}

// myPow 计算 x^n;  这种写法貌试可以 但是timeout
func myPow(x float64, n int) float64 {
	// 幂运算的两种特殊情况
	if n == 0 {
		return 1
	}

	if n == 1 {
		return x
	}

	tmp := n
	var xx float64 = 1
	if tmp < 0 {
		tmp = -n
	}

	for i := 0; i < tmp; i++ {
		xx = xx * x
	}

	// n 都是整数
	if n < 0 {
		xx = 1 / xx
	}

	r, _ := strconv.ParseFloat(fmt.Sprintf("%.5f", xx), 64)
	return r
}

// x = 2.00000, n = 10
// x = 2.10000, n = 3
// x = 2.00000, n = -2
func TestMyPow(t *testing.T) {
	pow1 := myPow(2.00000, 10)
	fmt.Println(pow1)

	pow2 := myPow(2.10000, 3)
	fmt.Println(pow2)

	pow3 := myPow(2.00000, -2)
	fmt.Println(pow3)
}

// 看了别的计算方法 使用折半计算 然后看看java Math.pow() 函数又是如何实现的;
func pow(x float64, n int) float64 {

	return x
}

func TestAtoi3(t *testing.T) {
	if atoi, err := strconv.Atoi("+-442"); err != nil {
		fmt.Println(atoi)
	}
}

// 判断一个整型是不是 回文数
func TestIsPalindrome(t *testing.T) {
	//p0:= IsPalindrome2(10)
	//fmt.Println(p0)

	p1 := IsPalindrome2(121)
	fmt.Println(p1)

	//p2:= IsPalindrome2(-121)
	//fmt.Println(p2)
}

// 整型转成罗马
func TestIntToRoman(t *testing.T) {
	roman := IntToRoman(1994)
	fmt.Println(roman)
}

// 罗马转整型
func TestRomanToInt(t *testing.T) {
	toInt := RomanToInt("MCMXCIV")
	fmt.Println(toInt)
}

// 三数之和
func TestThreeSum(t *testing.T) {
	// [-2,0,0,2,2]

	nums := []int{-1, 0, 1, 2, -1, -4}
	//nums := []int{-2, 0, 0, 2, 2}
	//nums := []int{0, 0, 0}

	sum := ThreeSum(nums)
	fmt.Println(sum)
}

func TestThreeSumClosest(t *testing.T) {
	var (
		nums = []int{-1, 2, 1, -4}
		// -4 -1 1 2
		target = 1
	)

	closest := ThreeSumClosest(nums, target)
	fmt.Println(closest)
}

func TestFourSum(t *testing.T) {
	var (
		//nums = []int{1,0,-1,0,-2,2}
		nums   = []int{-3, -1, 0, 2, 4, 5}
		target int
	)

	sum := FourSum(nums, target)
	fmt.Println(sum)
}
