package bit

import (
	"fmt"
	"testing"
)

// 今天看了二进制的一个算法, 老鼠试毒 的问题

// Question description: 如果有8杯水 只有一杯有毒  问需要多少只老鼠 可以把毒试出来;
// 如何使用计算机  来  模拟人的思考, 以及使用 二进制 计算的特性;
// todo: bitmask 的使用  名词  位掩码  的使用
// todo: 涉及了线性代数中 矩阵的转置;

func TestMouseTryPoison(t *testing.T) {
	// todo:
}

var (
	GOOD   = 1
	BETTER = 2
	BEST   = 4
)

func TestCompetence(t *testing.T) {
	defineCompetence()
}

// 使用bitmap可以快速进行算法计算;
func defineCompetence() {
	// 111
	fmt.Println(GOOD | BETTER | BEST)
}

// 快速乘法
func TestFastMulti(t *testing.T) {
	multi := FastMulti(2, 7)
	fmt.Println(multi)
}
