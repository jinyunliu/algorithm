package bit

// FastMulti 使用位快乘  return v1 * v2  一个数/2 一个数乘2 最后让一个数表示乘积
// 这是一个模板
func FastMulti(v1, v2 int) int {
	res := 0
	for v2 > 0 {
		if (v2 & 1) == 1 {
			res += v1
		}
		v2 >>= 1
		v1 <<= 1
	}
	return res
}

// 2 4
// 4 2
// 8 1
// 2 5  101 2(4 + 1)
