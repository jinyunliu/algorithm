package tree

import (
	"fmt"
	"testing"
)

// 树 二叉树  是一套完整的数据结构理论 还需要其他基础项的联系 然后再回来继续写红黑树
// 一些其他的基本算法  B-Tree, 等等的 基本的树算法,  节点的旋转等.

// 红黑树的学习  数学归纳法进行证明下面的逻辑  数据归纳法是数学证明常用的方法 另一种方法是反证法
// 1 节点有两种颜色 不是黑色就是红色 树 父节点->子节点 是二叉树的一种
// 2 要求根节点是黑色 叶子节点null 是黑色
// 3 所有路径上的黑高 保证相同, 即黑色节点的数量一定相同, 为了这个默认构造的节点是 红色
//
// 4 红黑树的有点: 首先是一种平衡树, left < root < right, 中序遍历保证有序 left root right 递归遍历
//

// 首先定义一个结构的行为 抽象成 接口
type ITree interface {

	// Put 添加一个元素 返回这个添加节点的副本值 once put, 这个值实际是不允许修改 不然会设计 ReTree
	Put(val int)

	// Evict 移除一个元素 返回这个元素的节点信息, 同样返回一个副本
	Evict(val int) int

	// Contain 判断当前的tree中是否存在 这个值
	Contain(val int) bool

	// Size 查询这个树的size 这个是另一个结构了
	Size() int

	// Traversal 中序遍历这个红黑树 不用参数 直接遍历它本身
	Traversal()
}

// Comparable 因为是一种平衡树 需要比较查找位置 需要我们定义一个比较的标准 类似 Comparable
// 为了方便 我们直接使用int的值 每一个TreeNode中就存一个int的值 用于比较
type Comparable interface {
	Compare(val int) int
}

// 先定义红黑树的节点
type TreeNode struct {
	// 默认节点是 红色
	Red bool

	// 节点的数据类型  这里使用int类型 可以使用任意类型 如果使用的go 就是一个interface{} 接口 如果是java可以是object 或者 Generic T
	Val int
	// 定义树的左右孩子节点 和 当前节点的父节点
	Parent *TreeNode
	Left   *TreeNode
	Right  *TreeNode
}

// 定义树结构 下面的方法应该是针对 Bree 而不是 Node
type BRTree struct {
	// 保存底层存储结构的根节点
	Root *TreeNode
	// 保存树的大小
	Size int
}

// NewBRTree 就是红黑树的构造方法
func NewBRTree() *BRTree {
	return new(BRTree)
}

// NewTree Go不支持泛型 真的难受啊, 都没有办法约束 一个对象的行为 保证创建的一个节点保证是 pure node
// pure, not mixed, 保证节点的干净性  还有一个类似的adj, raw 表示原始的数据, 没有process.
func NewTree(val int) *TreeNode {
	return &TreeNode{false, val, nil, nil, nil}
}

// NewTreeNode 创建一个干净的节点
func NewTreeNode(val int) *TreeNode {
	return &TreeNode{true, val, nil, nil, nil}
}

// Put 插入一个节点 这是一个核心方法 先找到它的位置
// 根据我们写的 1 2 3 4 5 这 5个数的插入操作
func (t *BRTree) Put(val int) {
	// 因为我们是探索自己实现红黑树 可以根据我们分析 adherence:
	root := t.Root

	// lazily instantiate the tree
	if root == nil {
		root = NewTree(val)
	} else {
		// 已经创建好了树 应该查找, 应该 这个红色的节点应该插在 那个节点上的那个分支.
		// 这里需要 查找所有 节点 right next 的 为nil;  递归比较 返回 合适的 父节点

		//
	}

	// 首先插入一个 如何使用一个的特例 完成普遍性的 操作??? 这个是核心?
	// 需要学习 前人的方法论
	//if root.Val == val {
	//	return *t
	//} else if root.Val > val {
	//	// 构建左节点
	//	left := NewTreeNode(val)
	//	root.Left, left.Parent = left, root
	//	return *left
	//} else {
	//	// 构建右节点
	//	right := NewTreeNode(val)
	//	root.Right, right.Parent = right, root
	//	return *right
	//}
}

// findParentNode 查找父节点
func findParentNode(val int, root *TreeNode) *TreeNode {
	// todo: 查找符合条件的节点 这个看下hashMap的源码把

	return root
}

func (t *BRTree) Evict(val int) int {
	//
	panic("implement me")
}

func (t *BRTree) Contain(val int) bool {
	//
	panic("implement me")
}

// Traversal 中序遍历 红黑树 直接输出
func (t *BRTree) Traversal() {
	// delegate to it.
	t.Root.Traversal()
}

func (t *TreeNode) Traversal() {
	// 递归遍历 返回的边界是 t == nil
	if t == nil {
		return
	}

	// 递归遍历左子树
	t.Left.Traversal()
	// 输出当前的值
	fmt.Println(t.Val)
	// 递归遍历右子树
	t.Right.Traversal()
}

// find 递归查询
func find(val int, root *TreeNode) *TreeNode {
	return root
}

// TestBRTree 对 手写的红黑树 测试
func TestBRTree(t *testing.T) {
	tree := NewBRTree()
	fmt.Println(tree)
}
