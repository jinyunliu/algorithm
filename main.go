package main

import (
	"fmt"
)

func main() {
	// 任意定义的一个数组
	arr := []int{1, 2, 4, 5, 7, 8}
	mid := (len(arr) - 1) >> 1 // 长度/2 == 最后一个值的索引/2 就 +-1 对于二分操作来说都是无意义的;
	// 这里如果长度是奇数 直接/2 就是中位数; 如果是偶数 n/2 + n/2-1
	fmt.Printf("length: %d, 中心索引值：%d \n", len(arr), mid)

	midEqu := len(arr) >> 1
	fmt.Println(midEqu)

	fmt.Println(arr[:mid])
	fmt.Println(arr[mid:])

	fmt.Print("hello world")

	// 查询每个班级 分数最高的学生信息

	// 移位运算大于加减运算, 这个数load到cpu, 会首先执行cpu移位指令
	k := 4 - 2>>1
	fmt.Println(k)

	fmt.Println(10 % 10)

	s := "hello world"
	for i := 0; i < len(s); i++ {
		fmt.Println(s[i])
	}

	// 空串的长度为0
	fmt.Println(len(""))

	fmt.Println(2 | 1)
}
